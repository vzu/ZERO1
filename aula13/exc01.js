barbie = {
    titulo: 'barbie',
    direçao: 'Greta Gerwig',
    ano: 2023,
    elenco: ['Margot Robbie', 'Ryan Gosling', 'Issa Rae'],
    jahvi: false
}
console.log(
    `
    filme: ${barbie.titulo}
    ano: ${barbie.ano}
    direçao: ${barbie.direçao}
    elenco: ${barbie.elenco}
    ${barbie.jaVi ? 'sim, ja vi' : 'Nao vi ainda.'}
    `
)

