const rl = require('readline-sync')

const ano = rl.question('ano: ')
const nome = rl.question('nome: ')
const idade = rl.question('idade: ')
const email = rl.question('E-mail:')


/*

    question() retorna uma String.


    Por isso precisamos converter
    ano e idade
    para um valor numérico, para
    podermos fazer operaçoẽs
    matematicas com eles
    Para isso usamos Number()
*/

const anoDeNascimento = Number(ano) - Number(idade)

console.log(`${nome},${email},${idade},${anoDeNascimento}`)

