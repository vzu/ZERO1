const segredo = Math.floor(Math.random()*10)
const rl = require('readline-sync')
let numero

while(numero != segredo) {
    numero = Number(rl.question('acerte o numero secreto entre 1 e 10\n\t'))
    if (numero < segredo)
    console.log('tente um numero maior')
    if (numero > segredo)
    console.log('tente um numero menor')       
}
console.log('boa, você acertou!!!')
console.log(`O numero era ${segredo}`);