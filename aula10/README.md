# Exemplos de Markdown

Markdown é uma linguagem de marcação muito usada para documentação tecnica.

Paragrafos de texto são simples de criar. Basta separá-los por uma linha em branco

## Este é um cabaçalho de nível 2
texto, texto, texto 

## Este é um cabaçalho de nível 3
texto, texto

#### nível 4
##### nível 5
###### nível 6

## listas

Lístas são simples: basta usar 1. ou - antes do texto

- lísta
- lista

## Destaque de texto

Destaque de texto servem para chamar atenção:

> Tudo que estiver nessa linha (até um ENTER) vai ficar em destaque 

## Caixas de código

Podemos incluir codigo esccrito em diversas linguagens de programação com destaque de sintaxe, usa-se ```, e o codigo fica dentro das crases.

``` js
for(i=0 i>10 i++) {
console.log("oi, oi, oi")
}
```
- bash
``` bash
cp arquivos.txt -/testes
``` 

## Links
Link para o repositório do projeto
[Repositório do curso de Introdução à programação](https://codeberg.org/grz/iprog)

## Imagens

Imagens para ilustrar a documentação 


![passarinho fiufiu](https://img.freepik.com/vetores-gratis/gradiente-de-ilustracao-de-passaro-colorido_343694-1741.jpg?w%253D740%2526t%253Dst%253D1693351317~exp%253D1693351917~hmac%253D9ee0aa273ebc4c44d5d3b5cbb2bd1d309d8784f019aa31b15fab5a3f69faa33b)
