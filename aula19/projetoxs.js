const rl = require('readline-sync')
const Compras = ['Arroz', 'Feijão', 'Carne', 'Papel toalha', 'Macarrão', 'Detergente']

function listar() {
    console.table(Compras)
}

function cadastrar() {
    const nome = rl.question('Nome do item: ')
    Compras.push(nome)
    console.log('Ok')
    rl.question('Pressione ENTER pra continuar...')
}

function modificar() {
    const indice = rl.question("Indice da compra a modificar: ")
    const novoValor = rl.question("Novo item: ")
    Compras[indice] = novoValor
    console.log("Ok.")
    rl.question('Pressione ENTER pra continuar...')
}

function excluir() {
    const indice = rl.question("Indice da compra a excluir: ")
    const dadoExcluido = Compras.splice(indice,1)
    console.log(`Ok. Excluido ${dadoExcluido}`)
    rl.question('Pressione ENTER pra continuar...')
}

while(true) {

    console.log(
`
MENU
----
(L)istar
(C)adastrar
(M)odificar
(E)xcluir
(S)air
`        
)
    const opcao = rl.question("Opcao: ").toUpperCase()

    if (opcao == 'L') {
        listar()
    } else if(opcao == 'C') {
        cadastrar()
    } else if(opcao == 'M') {
        modificar()
    } else if (opcao == 'E') {
        excluir()
    } else if (opcao == 'S') {
        break
    } else {
        console.log('Opcao invalida')
    }


}

console.log("FIM DO PROGRAMA:")
