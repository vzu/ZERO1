const rl = require('readline-sync')

const tarefas = ['fazer aplicação','estudar para prova', 'brincar com o irmão']

function cadastrar() {
    const tarefa = rl.question('Nova tarefa:')
    tarefas.push(tarefa)
    console.log('cadastrada')
}

function listar() {
    console.table(tarefas)
}

function excluir() {
    console.table(tarefas)
    const indice = rl.question('indice da tarefa : ')
    tarefas.slice(indice, 1)
    console.log('tarefa excluida')
}

function modificar() {
    console.table(tarefas)
    const indice = rl.question('qual tarefa: ')
    const tarefaExcluida = tarefas.splice(indice,1)
    console.log('')
}

while(true) {
console.log(
`
MENU
----
(C)Cadastrar
(L)Listar
(E)Excluir
(M)Modificar
(S)Sair
`
)

const comando = rl.question('? ').toUpperCase()

if (comando == 'S') {
    break
} else if (comando == 'C') {
    cadastrar()
} else if (comando == 'L') {
    listar()
} else if (comando == 'E') {
    excluir()
} else if (comando == 'M') {
    modificar()
}

rl.question('tecle ENTER para continuar')
}

console.log('fim')
