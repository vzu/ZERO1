const rl = require('readline-sync')
const historicodeCompras = ['Arroz', 'feijão', 'tempero', 'suco']

process.on('exit', function (code) {
    console.log('fim')
});

function cadastrar() {
    console.log('cadastre sua compra:')
    const valItem = rl.question('Item:')
    const valPrioridade = rl.question('Prioridade (A, B , C): ')

    historicodeCompras.push(
        {
            Item: valItem,
            Prioridade: valPrioridade
        }
    )
    console.log('Item cadastrado')
}

function listar() {
    console.table(historicodeCompras)   
}

function excluir() {
    console.table(historicodeCompras)
    const indice = rl.question('indice da compra:')
    const ItemExcluido = historicodeCompras.splice(indice, 1)
    console.log(`Exclui: ${ItemExcluido[0].historicodeCompras}`)
}

function modificar() {
    console.table(historicodeCompras)
    const indice = rl.question('qual item?: ')
    const campo = rl.question('modificar qual campo?: ')
}

while(true) {

    console.log(
`
MENU
----
(L)istar
(C)adastrar
(M)odificar
(E)xcluir
(S)air
`        
)
    const opcao = rl.question("Opcao: ").toUpperCase()

    if (opcao == 'L') {
        listar()
    } else if(opcao == 'C') {
        cadastrar()
    } else if(opcao == 'M') {
        modificar()
    } else if (opcao == 'E') {
        excluir()
    } else if (opcao == 'S') {
        break
    } else {
        console.log('Opcao invalida')
    }


}